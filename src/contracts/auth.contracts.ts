import { Type } from 'class-transformer';
import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength, ValidateNested } from 'class-validator';
import { BaseResponseDTO } from './base.contracts';

export class AuthSignupRequest {
  @IsNotEmpty()
  @IsEmail({}, { message: 'Please provide a valid email address' })
  email!: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  fullName!: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(20, { message: 'Password must not exceed 20 characters long' })
  password!: string;
}

export class AuthSignupResponse extends BaseResponseDTO {
  @IsNotEmpty()
  @IsString()
  token!: string;
}

export class AuthLoginRequest {
  @IsNotEmpty()
  @IsEmail({}, { message: 'Please provide a valid email address' })
  email!: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8, { message: 'Password must be at least 8 characters long' })
  @MaxLength(20, { message: 'Password must not exceed 20 characters long' })
  password!: string;
}

export interface TokenPayload {
  email: string;
  fullName: string;
}

export class AuthLoginResponseDTO {
  @IsNotEmpty()
  @IsString()
  _id!: string;

  @IsNotEmpty()
  @IsEmail()
  email!: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  fullName!: string;
}

export class AuthLoginResponse extends BaseResponseDTO {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => AuthLoginResponseDTO)
  result!: AuthLoginResponseDTO;
}
