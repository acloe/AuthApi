import { getModelForClass, prop, modelOptions, Severity, pre, DocumentType } from '@typegoose/typegoose';

import * as crypto from '../../utils/crypto';

/* Typegoose Hook to hash password and save into db record */
@pre<UserCollection>('save', function () {
  if (this.isModified('password') || this.isNew) {
    this.password = crypto.encryptPassword(this.password);
  }
})
/* Declare user class */

@modelOptions({
  options: { allowMixed: Severity.ALLOW, customName: 'user' },
})
export class UserCollection {
  @prop({ required: true, minlength: 3 })
  public fullName!: string;

  @prop({ required: true, unique: true })
  public email!: string;

  @prop({ required: true, minlength: 8, maxlength: 20 })
  public password!: string;

  validatePassword(this: DocumentType<UserCollection>, inputPassword: string) {
    return crypto.validPassword(inputPassword, this.password);
  }
}

export const UserModel = getModelForClass(UserCollection);
