import _ from 'lodash';
import { BadRequestError } from 'routing-controllers';
import { AuthLoginRequest, AuthLoginResponseDTO } from '../../../contracts';
import ErrorHandler, { ConflictError } from '../../../middlewares/errors';
import logger from '../../../utils/logger';
import { UserModel } from '../../models/user.model';

export class LoginService {
  public async loginUser(data: AuthLoginRequest): Promise<AuthLoginResponseDTO> {
    try {
      if (_.isEmpty(data)) throw new BadRequestError('Login Payload is required');

      const userDocument = await UserModel.findOne({ email: data.email });

      // check if user exist
      if (!userDocument) throw new ConflictError('Invalid Login Ceredentials');

      // check if password is valid
      const passwordMatch = userDocument.validatePassword(data.password);

      if (!passwordMatch) throw new ConflictError('Invalid Login Ceredentials');

      // STRIPING OFF PASWORD
      const { email, fullName, _id } = userDocument;

      const loginResponse: AuthLoginResponseDTO = {
        _id: _id.toString(),
        email,
        fullName,
      };

      return loginResponse;
    } catch (err) {
      logger.error(`[LoginService:createUserAccount]: ${err}`);
      throw new ErrorHandler(err);
    }
  }
}
