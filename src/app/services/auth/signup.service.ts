import _ from 'lodash';

import ErrorHandler, { ConflictError, UnprocessableEntityError } from '../../../middlewares/errors';
import { TokenPayload } from '../../../contracts';

import jwt from 'jsonwebtoken';

import { AuthSignupRequest } from '../../../contracts/auth.contracts';
import { UserModel } from '../../models/user.model';
import logger from '../../../utils/logger';
import config from '../../../utils/config';
import { BadRequestError } from 'routing-controllers';

export class SignupService {
  public async createUserAccount(data: AuthSignupRequest): Promise<string> {
    try {
      if (_.isEmpty(data)) throw new BadRequestError('User Resgistration payload is required');

      const emailExist = await UserModel.findOne({ email: data.email });

      if (emailExist) throw new ConflictError('Email is in use , Login instead');

      const newUser = await UserModel.create(data);

      if (!newUser) throw new UnprocessableEntityError('There was a problem creating your account');

      const { email, fullName, _id } = newUser;

      const payload: TokenPayload = {
        email,
        fullName,
      };

      const token = jwt.sign(payload, config.JWT_SECRET, { expiresIn: config.TOKEN_EXPIRE });

      if (!token) {
        await UserModel.findByIdAndRemove(_id);
        throw new UnprocessableEntityError('Error generating JWT token');
      }

      return token;
    } catch (err) {
      logger.error(`[SignupService:createUserAccount]: ${err}`);
      throw new ErrorHandler(err);
    }
  }
}
