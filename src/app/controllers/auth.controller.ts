import { Body, HttpCode, JsonController, Post } from 'routing-controllers';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';

import { AuthLoginRequest, AuthSignupRequest, AuthSignupResponse, AuthLoginResponse } from '../../contracts';
import { SignupService, LoginService } from '../services/auth/index.service';

@JsonController('/auth')
export class AuthController {
  private signupService = new SignupService();
  private loginService = new LoginService();

  @Post('/register')
  @HttpCode(201)
  @OpenAPI({ summary: 'Create User Account', description: 'User Account Registration ' })
  @ResponseSchema(AuthSignupResponse)
  async createUserAccount(
    @Body({ validate: true, required: true }) data: AuthSignupRequest
  ): Promise<AuthSignupResponse> {
    const token = await this.signupService.createUserAccount(data);
    return { message: 'Successfully Created User Account', token };
  }

  @Post('/login')
  @HttpCode(200)
  @OpenAPI({
    summary: 'Log Users Into their Accounts',
    description: 'Provide both email and a valid password`',
  })
  @ResponseSchema(AuthLoginResponse)
  async loginUser(@Body({ required: true }) loginPayload: AuthLoginRequest): Promise<AuthLoginResponse> {
    const result = await this.loginService.loginUser(loginPayload);
    return { message: 'Successfully Logged User In', result };
  }
}
